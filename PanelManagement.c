///////////////////////////////////////////////////////////////////////////////
// MODULE	PanelManagement
// PURPOSE	Some functions shared by all panels.
//			Also contains function to handle panels in tabs
///////////////////////////////////////////////////////////////////////////////

#include <iso646.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

#include <utility.h>
#include <userint.h>
#include <toolbox.h>

//#include "ShowAttributes.h"				// Optional for debugging
#include "PanelManagement.h"

char ScreenShotDir[MAX_PATHNAME_LEN]="";	// Will be prompted the 1st time if empty

char SelectedTypeface[256]="";
int  SelectedFontSize=11;

static char *StrClip=NULL;

#if _CVI_ < 810 	// Panel sizing event work only in recent versions
	#define EVENT_PANEL_SIZING         37  /* eventData1 = sizing edge (PANEL_SIZING_TOPLEFT, etc) */
	#define EVENT_MOUSE_WHEEL_SCROLL   36
	#define MOUSE_WHEEL_PAGE_UP         0
	#define MOUSE_WHEEL_PAGE_DOWN       1
	#define MOUSE_WHEEL_SCROLL_UP       2
	#define MOUSE_WHEEL_SCROLL_DOWN     3
#endif

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Add some info about a control to a dynamic string
/// HIRET	0 if no error, ENOMEM otherwise
///////////////////////////////////////////////////////////////////////////////
static int AddToClip(int Panel, int Control) {
	char ConstName[80], Label[255], CbName[80];
	int Style, Type, sz;

	int St = SetBreakOnLibraryErrors (0);
	GetCtrlAttribute(Panel, Control, ATTR_CONSTANT_NAME, ConstName);
	GetCtrlAttribute(Panel, Control, ATTR_LABEL_TEXT,    Label);
	GetCtrlAttribute(Panel, Control, ATTR_CTRL_STYLE,   &Style);
	GetCtrlAttribute(Panel, Control, ATTR_DATA_TYPE,    &Type);
	GetCtrlAttribute(Panel, Control, ATTR_CALLBACK_NAME, CbName);
	SetBreakOnLibraryErrors (St);

	sz=snprintf(NULL, 0, "%s\t\"%s\"\t%d\t%d\t%s\n",
				ConstName, Label, Style, Type, CbName);
	if (NULL==(StrClip = realloc (StrClip, strlen(StrClip)+(unsigned int)sz+1))) return ENOMEM;
	snprintf(StrClip+strlen(StrClip), (size_t)sz+1, "%s\t\"%s\"\t%d\t%d\t%s\n",
			 ConstName, Label, Style, Type, CbName);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Handle function keys common between panels
/// HIFN	Just insert the following line in your panel callbacks:
/// HIFN	case EVENT_KEYPRESS: return HandlePanelDefaultKeys(panel, eventData1); break;
/// HIFN	OBSOLETE: was using outdated macros, has problems on Linux and doesn't work in UTF-8
/// HIPAR	panel/handle of the calling panel
/// HIPAR	eventData1/In case of EVENT_KEYPRESS
/// HIRET	If 1 is returned, you need to swallow the event
///////////////////////////////////////////////////////////////////////////////
int HandlePanelDefaultKeys(int panel, int eventData1) {
	int Bitmap=0, Err;
	char FileName[MAX_FILENAME_LEN]="", PathName[MAX_PATHNAME_LEN], *Pos=FileName;
	time_t Time;
	//struct tm TM;

	// No idea why bit 20 may be on. Let's clear it.
	// See http://forums.ni.com/t5/LabWindows-CVI/EVENT-KEYPRESS-in-Linux-not-consistant/m-p/1582008/highlight/true#M52925
	eventData1 &= ~(1<<20);

	switch (eventData1) {
		// Use [Ctrl][Shift][->] or Ctrl-] to navigate to the next panel
		case VAL_SHIFT_AND_MENUKEY | VAL_LEFT_ARROW_VKEY:
		case VAL_MENUKEY_MODIFIER | '[':
			ShowPreviousPanel(panel);
			return 1;

			// Use [Ctrl][Shift][<-] or Ctrl-[ to navigate to the previous panel
		case VAL_SHIFT_AND_MENUKEY | VAL_RIGHT_ARROW_VKEY:
		case VAL_MENUKEY_MODIFIER | ']':
			ShowNextPanel(panel);
			return 1;

			// Use [Ctrl][P] to print the current panel
		case VAL_MENUKEY_MODIFIER  | 'P':
		case VAL_MENUKEY_MODIFIER  | 'p':
			PrintPanel (panel, "", 1, VAL_VISIBLE_AREA, 1);
			return 1;

			// Use [Ctrl][B] to save a bitmap of the current panel
		case VAL_MENUKEY_MODIFIER  | 'B':
		case VAL_MENUKEY_MODIFIER  | 'b':
			if (0>GetPanelDisplayBitmap (panel, VAL_VISIBLE_AREA, VAL_ENTIRE_OBJECT, &Bitmap)) {
				MessagePopup("Error", "Could not take screenshot of panel");
				break;
			}
			if (ScreenShotDir[0]=='\0')
				if (VAL_DIRECTORY_SELECTED!=DirSelectPopup (ScreenShotDir,
					"Select Directory where to save the screenshots", 1, 1, ScreenShotDir))
					break;
				Time=time(NULL);
			strftime (FileName, 80, "Screenshot-%Y%m%d-%H%M%S-", localtime/*_r*/(&Time /*, &TM*/));
			GetPanelAttribute (panel, ATTR_TITLE, FileName+strlen(FileName));
			strcat(FileName, ".png");
			do { if (isspace(*Pos)) *Pos='_'; } while (*++Pos!='\0');
			MakePathname (ScreenShotDir, FileName, PathName);
			Err=SaveBitmapToPNGFile (Bitmap, PathName);
			if (Err<0) MessagePopup("Error saving screenshot", GetUILErrorString(Err));
			DiscardBitmap(Bitmap);
		return 1;

		// Use [Ctrl][Shift][F] to change the font used on all controls of that panel / entire application
		case VAL_SHIFT_AND_MENUKEY | 'F':
		case VAL_MENUKEY_MODIFIER  | 'F':				// Linux
			#pragma clang diagnostic push
			#pragma clang diagnostic ignored "-Winvalid-source-encoding"
			if (FontSelectPopup ("Select a base font",
				"S�mpl� T�xt O01Il2Z3E ~!@#$ {}[]()", 0, SelectedTypeface, 0, 0,
								 0, 0, 0, 0, &SelectedFontSize, 6, 48, 1, 1)) {
				CreateMetaFontWithCharacterSet ("MyFontBase", SelectedTypeface, SelectedFontSize,
												0, 0, 0, 0, 0, VAL_NATIVE_CHARSET);
				ReplaceFont(0 /*panel*/, NULL, "MyFontBase");
								 }
								 #pragma clang diagnostic pop
								 return 1;

								 // Use [Ctrl][Shift][T] to get a clipboard full of text info about the panel
		case VAL_SHIFT_AND_MENUKEY  | 'T':
		case VAL_MENUKEY_MODIFIER   | 'T': {
			char ConstName[80], Title[255], CallBack[80];
			int Parent;
			GetPanelAttribute (panel, ATTR_CONSTANT_NAME, ConstName);
			GetPanelAttribute (panel, ATTR_TITLE,         Title);
			GetPanelAttribute (panel, ATTR_PANEL_PARENT, &Parent);
			GetPanelAttribute (panel, ATTR_CALLBACK_NAME, CallBack);
			if (NULL==(StrClip=malloc(100))) return ENOMEM;
			sprintf(StrClip, "Panel %s \"%s\", Parent=%d, CallBack=%s\n"
			"Const\tLabel\tStyle\tType\tCallBack\n",
		   ConstName, Title, Parent, CallBack);
			FindAllCtrls(panel, AddToClip);
			#ifdef _NI_linux_
			fprintf(stderr, "%s", StrClip);	// Dump to stderr
			#endif
			ClipboardPutText (StrClip);		// Doesn't do anything on Linux
			free(StrClip); StrClip=NULL;
			return 1;
		}

		// Use [Ctrl][0] to center the current panel
		case VAL_MENUKEY_MODIFIER  | '0':
			CenterPanel(panel);
			return 1;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Convert an eventData2 from EVENT_KEYPRESS into something usable
/// HIFN	Note, you can use the macro CVI_UTF8 to know if a CVI 2020 exe is in UTF-8
/// HIFN	but this code should work in both ANSI and UTF-8
/// HIPAR	eventData2/As received by the callback function
/// HIRET	A usable keycode (see all the VAL_*KEY* macros)
///////////////////////////////////////////////////////////////////////////////
int GetMyKey(int eventData2) {
	if (KeyPressEventIsLeadByte(eventData2)) return 0;
	int modifierKey = GetKeyPressEventModifiers(eventData2)
					  & ~(1<<20);	// This is a bug on Linux
	if (modifierKey<0) return modifierKey;
	int virtualKey = GetKeyPressEventVirtualKey(eventData2);
	if (virtualKey<0) return virtualKey;
	if (virtualKey==0) {
		int Key=GetKeyPressEventCharacter (eventData2);
		if (Key<=0) return Key;
		return        Key | modifierKey;
	}
	else return virtualKey | modifierKey;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Handle function keys common between panels (both ANSI and UTF-8)
/// HIFN	Just insert the following lines in your panel callbacks:
/// HIFN	case EVENT_KEYPRESS: switch (GetMyKey(eventData2)) {...CASE local keys...}; return HandlePanelDefaultKeysExt(panel, eventData1, eventData2);
/// HIPAR	panel/handle of the calling panel
/// HIPAR	eventData1/Unused, but here to avoid confusion with HandlePanelDefaultKeys()
/// HIPAR	eventData2/As received by the callback function
/// HIRET	If 1 is returned, you need to swallow the event
///////////////////////////////////////////////////////////////////////////////
int HandlePanelDefaultKeysExt(int panel, int eventData1, int eventData2) {
	int Bitmap=0, Err;
	char FileName[MAX_FILENAME_LEN]="", PathName[MAX_PATHNAME_LEN], *Pos=FileName;
	time_t Time;
	//struct tm TM;

	// No idea why bit 20 may be on. Let's clear it.
	// See http://forums.ni.com/t5/LabWindows-CVI/EVENT-KEYPRESS-in-Linux-not-consistant/m-p/1582008/highlight/true#M52925
	//eventData1 &= ~(1<<20);	// TODO: verify in Linux

	switch (GetMyKey(eventData2)) {
		// Use [Ctrl][Shift][->] or Ctrl-] to navigate to the next panel
		// NOTE: Ctrl-Shift-arrow seems to interfere with tabs where it's already use to move between tabs
		case VAL_SHIFT_AND_MENUKEY | VAL_LEFT_ARROW_VKEY:
		case VAL_MENUKEY_MODIFIER | '[':
			ShowPreviousPanel(panel);
			return 1;

			// Use [Ctrl][Shift][<-] or Ctrl-[ to navigate to the previous panel
		case VAL_SHIFT_AND_MENUKEY | VAL_RIGHT_ARROW_VKEY:
		case VAL_MENUKEY_MODIFIER | ']':
			ShowNextPanel(panel);
			return 1;

			// Use [Ctrl][P] to print the current panel
		case VAL_MENUKEY_MODIFIER  | 'P':
		case VAL_MENUKEY_MODIFIER  | 'p':
			PrintPanel (panel, "", 1, VAL_VISIBLE_AREA, 1);
			return 1;

			// Use [Ctrl][B] to save a bitmap of the current panel
		case VAL_MENUKEY_MODIFIER  | 'B':
		case VAL_MENUKEY_MODIFIER  | 'b':
			if (0>GetPanelDisplayBitmap (panel, VAL_VISIBLE_AREA, VAL_ENTIRE_OBJECT, &Bitmap)) {
				MessagePopup("Error", "Could not take screenshot of panel");
				break;
			}
			if (ScreenShotDir[0]=='\0')
				if (VAL_DIRECTORY_SELECTED!=DirSelectPopup (ScreenShotDir,
					"Select Directory where to save the screenshots", 1, 1, ScreenShotDir))
					break;
				Time=time(NULL);
			strftime (FileName, 80, "Screenshot-%Y%m%d-%H%M%S-", localtime/*_r*/(&Time /*, &TM*/));
			GetPanelAttribute (panel, ATTR_TITLE, FileName+strlen(FileName));
			strcat(FileName, ".png");
			do { if (isspace(*Pos)) *Pos='_'; } while (*++Pos!='\0');
			MakePathname (ScreenShotDir, FileName, PathName);
			Err=SaveBitmapToPNGFile (Bitmap, PathName);
			if (Err<0) MessagePopup("Error saving screenshot", GetUILErrorString(Err));
			DiscardBitmap(Bitmap);
		return 1;

		// Use [Ctrl][Shift][F] to change the font used on all controls of that panel / entire application
		case VAL_SHIFT_AND_MENUKEY | 'F':
		case VAL_MENUKEY_MODIFIER  | 'F':				// Linux
			#pragma clang diagnostic push
			#pragma clang diagnostic ignored "-Winvalid-source-encoding"
			if (FontSelectPopup ("Select a base font",
				"S�mpl� T�xt O01Il2Z3E ~!@#$ {}[]()", 0, SelectedTypeface, 0, 0,
								 0, 0, 0, 0, &SelectedFontSize, 6, 48, 1, 1)) {
				CreateMetaFontWithCharacterSet ("MyFontBase", SelectedTypeface, SelectedFontSize,
												0, 0, 0, 0, 0, VAL_NATIVE_CHARSET);
				ReplaceFont(0 /*panel*/, NULL, "MyFontBase");
								 }
								 #pragma clang diagnostic pop
								 return 1;

								 // Use [Ctrl][Shift][T] to get a clipboard full of text info about the panel
		case VAL_SHIFT_AND_MENUKEY  | 'T':
		case VAL_MENUKEY_MODIFIER   | 'T': {
			char ConstName[80], Title[255], CallBack[80];
			int Parent;
			GetPanelAttribute (panel, ATTR_CONSTANT_NAME, ConstName);
			GetPanelAttribute (panel, ATTR_TITLE,         Title);
			GetPanelAttribute (panel, ATTR_PANEL_PARENT, &Parent);
			GetPanelAttribute (panel, ATTR_CALLBACK_NAME, CallBack);
			if (NULL==(StrClip=malloc(100))) return ENOMEM;
			sprintf(StrClip, "Panel %s \"%s\", Parent=%d, CallBack=%s\n"
			"Const\tLabel\tStyle\tType\tCallBack\n",
		   ConstName, Title, Parent, CallBack);
			FindAllCtrls(panel, AddToClip);
			#ifdef _NI_linux_
			fprintf(stderr, "%s", StrClip);	// Dump to stderr
			#endif
			ClipboardPutText (StrClip);		// Bug: Doesn't do anything on Linux
			free(StrClip); StrClip=NULL;
			return 1;
		}

		// Use [Ctrl][0] to center the current panel
		case VAL_MENUKEY_MODIFIER  | '0':
			CenterPanel(panel);
			return 1;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Move to the previous visible panel with the same parent
///////////////////////////////////////////////////////////////////////////////
void ShowPreviousPanel(int panel) {
	int Next, Parent, First, Visible, Current=-1;
	GetPanelAttribute (panel, ATTR_PANEL_PARENT, &Parent);

	// Do not apply to tabs
	if (Parent and FindParentTab(panel, NULL)) { ShowPreviousPanel(Parent); return; }

	GetPanelAttribute (Parent, ATTR_FIRST_CHILD, &First);
	Next=First;
	do {
		GetPanelAttribute (Next, ATTR_VISIBLE, &Visible);
		if (Visible) Current=Next;
		GetPanelAttribute (Next, ATTR_NEXT_PANEL, &Next);
		if (Next==0) Next=First;
	} while (Next!=panel or Current<0);
		// SetPanelAttribute (Next, ATTR_ZPLANE_POSITION, 0);
		SetActivePanel(Current);
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Move to the next visible panel with the same parent
///////////////////////////////////////////////////////////////////////////////
void ShowNextPanel(int panel) {
	int Next, Parent, Visible;
	GetPanelAttribute (panel, ATTR_PANEL_PARENT, &Parent);

	// Do not apply to tabs
	if (Parent and FindParentTab(panel, NULL)) { ShowNextPanel(Parent); return; }

	Next=panel;
	do {
		GetPanelAttribute (Next, ATTR_NEXT_PANEL, &Next);
		if (Next==0) {
			GetPanelAttribute (panel,  ATTR_PANEL_PARENT, &Parent);
			GetPanelAttribute (Parent, ATTR_FIRST_CHILD,  &Next);
		}
		GetPanelAttribute (Next, ATTR_VISIBLE, &Visible);
	} while (!Visible and Next!=panel);
	// SetPanelAttribute (Next, ATTR_ZPLANE_POSITION, 0);
	SetActivePanel(Next);
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Center the panel in the closest monitor (only if visible)
///////////////////////////////////////////////////////////////////////////////
void CenterPanel(int panel) {
	int Monitor, Top, Left, Height, Width, PanelWidth, PanelHeight, Visible;
	if (panel==0) return;

	GetMonitorFromPanel(panel, &Monitor);
	GetMonitorAttribute(Monitor, ATTR_TOP,    &Top);
	GetMonitorAttribute(Monitor, ATTR_LEFT,   &Left);
	GetMonitorAttribute(Monitor, ATTR_WIDTH,  &Width);
	GetMonitorAttribute(Monitor, ATTR_HEIGHT, &Height);

	GetPanelAttribute(panel, ATTR_VISIBLE,    &Visible);
	if (Visible) {
		GetPanelAttribute(panel, ATTR_WIDTH,  &PanelWidth);
		GetPanelAttribute(panel, ATTR_HEIGHT, &PanelHeight);
		SetPanelPos (panel, Top+(Height-PanelHeight)/2, Left+(Width-PanelWidth)/2);
	}
}

///////////////////////////////////////////////////////////////////////////////
///	HIFN	Return the absolute screen position of a (possible child) panel
///////////////////////////////////////////////////////////////////////////////
void GetPanelAbsolutePos(int Panel, int* Top, int* Left) {
	int T, L;
	*Top=*Left=0;
	while (Panel!=0) {
		GetPanelAttribute(Panel, ATTR_TOP,  &T); *Top +=T;
		GetPanelAttribute(Panel, ATTR_LEFT, &L); *Left+=L;
		GetPanelAttribute(Panel, ATTR_PANEL_PARENT, &Panel);
	}
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Change a font with another in all controls of the panel
/// HIPAR	panel/Panel on which to change the font. 0 for all top panels
/// HIPAR	Find/Font to search and replace. NULL for any
/// HIPAR	ReplaceWith/Replacement font
///////////////////////////////////////////////////////////////////////////////
void ReplaceFont(int panel, char *Find, char *ReplaceWith) {
	int ChildPanel, Ctrl, Style;
	char Font[255]="";
	// Iterate on child panels
	GetPanelAttribute (panel, ATTR_FIRST_CHILD, &ChildPanel);
	while (ChildPanel!=0) {
		ReplaceFont(ChildPanel, Find, ReplaceWith);
		GetPanelAttribute (ChildPanel, ATTR_NEXT_PANEL, &ChildPanel);
	}

	// Just for possible menu bar
	int MenuBar = (panel==0 ? 0 : GetPanelMenuBar (panel));
	if (MenuBar) {
		if (Find==NULL)
			SetMenuBarAttribute (MenuBar, 0, ATTR_MENU_BAR_FONT, ReplaceWith);
		else {
			Font[0]='\0'; \
			GetMenuBarAttribute (MenuBar, 0, ATTR_MENU_BAR_FONT, Font);
			if (Font[0]!='\0' and strcmp(Font, Find)==0)
				SetMenuBarAttribute (MenuBar, 0, ATTR_MENU_BAR_FONT, ReplaceWith);
		}
	}

	int SBLE = SetBreakOnLibraryErrors (0);	// Because most fonts are not valid for most controls

	Font[0]='\0';
	if (Find==NULL) SetPanelAttribute(panel, ATTR_TITLE_FONT, ReplaceWith);
	else {
		GetPanelAttribute (panel, ATTR_TITLE_FONT, Font);
		if (Font[0]!='\0' and strcmp(Font, Find)==0) {
			fprintf(stderr, "\nReplacing font %s", Font);
			SetPanelAttribute(panel, ATTR_TITLE_FONT, ReplaceWith);
		}
	}

	//TRY(ATTR_PRINT_FONT_NAME);	// SetPrintAttribute

	// Now for all controls
	GetPanelAttribute (panel, ATTR_PANEL_FIRST_CTRL, &Ctrl);
	while (panel!=0 and Ctrl!=0) {
		GetCtrlAttribute (panel, Ctrl, ATTR_CTRL_STYLE, &Style);

		if (Style==CTRL_TABS) {	// Attribute can only be ATTR_LABEL_FONT
			int Attribute=ATTR_LABEL_FONT;
			Font[0]='\0';
			if (Find==NULL) {
				SetTabPageAttribute(panel, Ctrl, VAL_ALL_OBJECTS, Attribute, ReplaceWith);
			} else {
				GetCtrlAttribute (panel, Ctrl, Attribute, Font);
				if (Font[0]!='\0' and strcmp(Font, Find)==0) {
					fprintf(stderr, "\nReplacing font %s", Font);
					SetCtrlAttribute(panel, Ctrl, Attribute, ReplaceWith);
					SetTabPageAttribute(panel, Ctrl, VAL_ALL_OBJECTS, Attribute, ReplaceWith);
				}
			}
			goto Skip;	// No other attributes
		}

#define TRY(Attribute)	\
			Font[0]='\0';\
			if (Find==NULL) SetCtrlAttribute(panel, Ctrl, Attribute, ReplaceWith);\
			else { \
				GetCtrlAttribute (panel, Ctrl, Attribute, Font);\
				if (Font[0]!='\0' and strcmp(Font, Find)==0) {\
					fprintf(stderr, "\nReplacing font %s", Font); \
					SetCtrlAttribute(panel, Ctrl, Attribute, ReplaceWith);\
				} \
			}


		TRY(ATTR_LABEL_FONT);
		TRY(ATTR_TEXT_FONT);

		if (Style==CTRL_GRAPH or Style==CTRL_GRAPH_LS or
				Style==CTRL_DIGITAL_GRAPH or Style==CTRL_DIGITAL_GRAPH_LS or
				Style==CTRL_STRIP_CHART or Style==CTRL_STRIP_CHART_LS) {

			int Plot=0, NbTraces=0;
			TRY(ATTR_XYNAME_FONT);
			TRY(ATTR_XYLABEL_FONT);
			TRY(ATTR_XLABEL_FONT);
			TRY(ATTR_YLABEL_FONT);

			switch (Style) {
				case CTRL_GRAPH:
				case CTRL_GRAPH_LS:
					GetCtrlAttribute (panel, Ctrl, ATTR_FIRST_PLOT, &Plot);
					while (Plot) {
						Font[0]='\0';
						if (Find==NULL) SetPlotAttribute (panel, Ctrl, Plot, ATTR_PLOT_FONT,    ReplaceWith),
							   SetPlotAttribute (panel, Ctrl, Plot, ATTR_PLOT_LG_FONT, ReplaceWith);
						else {
							GetPlotAttribute (panel, Ctrl, Plot, ATTR_PLOT_FONT, Font);
							if (Font[0]!='\0' and strcmp(Font, Find)==0) {
								fprintf(stderr, "\nReplacing font %s", Font);
								SetPlotAttribute (panel, Ctrl, Plot, ATTR_PLOT_FONT,    ReplaceWith);
							}
							GetPlotAttribute (panel, Ctrl, Plot, ATTR_PLOT_LG_FONT, Font);
							if (Font[0]!='\0' and strcmp(Font, Find)==0) {
								fprintf(stderr, "\nReplacing font %s", Font);
								SetPlotAttribute (panel, Ctrl, Plot, ATTR_PLOT_LG_FONT, ReplaceWith);
							}
						}

						GetPlotAttribute (panel, Ctrl, Plot, ATTR_NEXT_PLOT, &Plot);
					}
					break;

				case CTRL_DIGITAL_GRAPH:
				case CTRL_DIGITAL_GRAPH_LS:
					TRY(ATTR_DIGWAVEFORM_FONT);
					break;

				case CTRL_STRIP_CHART:
				case CTRL_STRIP_CHART_LS:
					GetCtrlAttribute (panel, Ctrl, ATTR_NUM_TRACES, &NbTraces);
					for (int Trace=1; Trace<=NbTraces; Trace++) {
						Font[0]='\0';
						if (Find==NULL) SetTraceAttributeEx (panel, Ctrl, Trace, ATTR_TRACE_LG_FONT, ReplaceWith);
						else {
							GetTraceAttribute (panel, Ctrl, Trace, ATTR_TRACE_LG_FONT, Font);
							if (Font[0]!='\0' and strcmp(Font, Find)==0) {
								fprintf(stderr, "\nReplacing font %s", Font);
								SetTraceAttributeEx (panel, Ctrl, Trace, ATTR_TRACE_LG_FONT, ReplaceWith);
							}
						}
					}
					break;
			}

			int NbAnnotations=0;
			GetCtrlAttribute (panel, Ctrl, ATTR_NUM_ANNOTATIONS, &NbAnnotations);
			for (int Annotation=1; Annotation<=NbAnnotations; Annotation++) {
				Font[0]='\0';
				if (Find==NULL) SetAnnotationAttribute (panel, Ctrl, Annotation, ATTR_ANNOTATION_CAPTION_FONT, ReplaceWith);
				else {
					GetAnnotationAttribute (panel, Ctrl, Annotation, ATTR_ANNOTATION_CAPTION_FONT, Font);
					if (Font[0]!='\0' and strcmp(Font, Find)==0) {
						fprintf(stderr, "\nReplacing font %s", Font);
						SetAnnotationAttribute (panel, Ctrl, Annotation, ATTR_ANNOTATION_CAPTION_FONT, ReplaceWith);
					}
				}
			}

		}

Skip:	GetCtrlAttribute (panel, Ctrl, ATTR_NEXT_CTRL, &Ctrl);
	}
	SetBreakOnLibraryErrors(SBLE);
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	For a given font attribute, set it for all subpanels and controls
/// HIPAR	panel/Panel on which to change the font. 0 for all top panels
/// HIPAR	FontAttr/One of several font attributes:
/// HIPAR	FontAttr/ATTR_MENU_BAR_FONT, ATTR_LABEL_FONT,   ATTR_TITLE_FONT,       ATTR_TEXT_FONT
/// HIPAR	FontAttr/ATTR_XYNAME_FONT,   ATTR_XYLABEL_FONT, ATTR_XLABEL_FONT,      ATTR_YLABEL_FONT
/// HIPAR	FontAttr/ATTR_PLOT_FONT,     ATTR_PLOT_LG_FONT, ATTR_DIGWAVEFORM_FONT, ATTR_TRACE_LG_FONT, ATTR_ANNOTATION_CAPTION_FONT
/// HIPAR	ReplaceWith/Replacement font
///////////////////////////////////////////////////////////////////////////////
void SetFontAttribute(int panel, int FontAttr, char *ReplaceWith) {
	int ChildPanel, Ctrl, Style;
	// Iterate on child panels
	GetPanelAttribute (panel, ATTR_FIRST_CHILD, &ChildPanel);
	while (ChildPanel!=0) {
		SetFontAttribute (ChildPanel, FontAttr, ReplaceWith);
		GetPanelAttribute(ChildPanel, ATTR_NEXT_PANEL, &ChildPanel);
	}

	// Just for possible menu bar
	if (FontAttr==ATTR_MENU_BAR_FONT) {
		int MenuBar = (panel==0 ? 0 : GetPanelMenuBar (panel));
		if (MenuBar) SetMenuBarAttribute (MenuBar, 0, ATTR_MENU_BAR_FONT, ReplaceWith);
		return;
	}

	int SBLE = SetBreakOnLibraryErrors (0);	// Because most fonts are not valid for most controls

	if (FontAttr==ATTR_TITLE_FONT) {
		SetPanelAttribute(panel, FontAttr, ReplaceWith);
		goto End;
	}

	//TRY(ATTR_PRINT_FONT_NAME);	// SetPrintAttribute

	// Now for all controls
	GetPanelAttribute (panel, ATTR_PANEL_FIRST_CTRL, &Ctrl);
	while (panel!=0 and Ctrl!=0) {
		GetCtrlAttribute (panel, Ctrl, ATTR_CTRL_STYLE, &Style);

		if (FontAttr==ATTR_LABEL_FONT and Style==CTRL_TABS) {	// Attribute can only be ATTR_LABEL_FONT
			int Attribute=ATTR_LABEL_FONT;
			SetTabPageAttribute(panel, Ctrl, VAL_ALL_OBJECTS, Attribute, ReplaceWith);
			goto Skip;	// No other attributes
		}

		if (FontAttr==ATTR_LABEL_FONT or FontAttr==ATTR_TEXT_FONT) {
			SetCtrlAttribute(panel, Ctrl, FontAttr, ReplaceWith);
			goto Skip;
		}

		if (Style==CTRL_GRAPH or Style==CTRL_GRAPH_LS or
				Style==CTRL_DIGITAL_GRAPH or Style==CTRL_DIGITAL_GRAPH_LS or
				Style==CTRL_STRIP_CHART or Style==CTRL_STRIP_CHART_LS) {

			int Plot=0, NbTraces=0;
			if (FontAttr==ATTR_XYNAME_FONT or FontAttr==ATTR_XYLABEL_FONT or FontAttr==ATTR_XLABEL_FONT or FontAttr==ATTR_YLABEL_FONT) {
				SetCtrlAttribute(panel, Ctrl, FontAttr, ReplaceWith);
				goto Skip;
			}

			switch (Style) {
				case CTRL_GRAPH:
				case CTRL_GRAPH_LS:
					GetCtrlAttribute (panel, Ctrl, ATTR_FIRST_PLOT, &Plot);
					if (FontAttr==ATTR_PLOT_FONT or FontAttr==ATTR_PLOT_LG_FONT) {
						while (Plot) {
							SetPlotAttribute (panel, Ctrl, Plot, FontAttr, ReplaceWith);
							GetPlotAttribute (panel, Ctrl, Plot, ATTR_NEXT_PLOT, &Plot);
						}
						goto Skip;
					}
					break;

				case CTRL_DIGITAL_GRAPH:
				case CTRL_DIGITAL_GRAPH_LS:
					if (FontAttr==ATTR_DIGWAVEFORM_FONT) {
						SetCtrlAttribute(panel, Ctrl, FontAttr, ReplaceWith);
						goto Skip;
					}
					break;

				case CTRL_STRIP_CHART:
				case CTRL_STRIP_CHART_LS:
					GetCtrlAttribute (panel, Ctrl, ATTR_NUM_TRACES, &NbTraces);
					if (FontAttr==ATTR_TRACE_LG_FONT) {
						for (int Trace=1; Trace<=NbTraces; Trace++)
							SetTraceAttributeEx (panel, Ctrl, Trace, FontAttr, ReplaceWith);
						goto Skip;
					}
					break;
			}

			int NbAnnotations=0;
			GetCtrlAttribute (panel, Ctrl, ATTR_NUM_ANNOTATIONS, &NbAnnotations);
			if (FontAttr==ATTR_ANNOTATION_CAPTION_FONT)
				for (int Annotation=1; Annotation<=NbAnnotations; Annotation++)
					SetAnnotationAttribute (panel, Ctrl, Annotation, FontAttr, ReplaceWith);
		}

Skip:	GetCtrlAttribute (panel, Ctrl, ATTR_NEXT_CTRL, &Ctrl);
	}
End:
	SetBreakOnLibraryErrors(SBLE);
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Dynamically change the fonts in use in all open panels
///////////////////////////////////////////////////////////////////////////////
void FixFonts(char *TypeFace, int Size) {

	// This tries and fails to work around a bug of ugly bold and italic fonts under Linux
	char Name[256];
	#define WhatFont(Font) \
	GetFontTypefaceName(Font, Name); \
	fprintf(stderr, "\n" Font ": %s", Name);

	WhatFont("NIDialogMetaFont");
	WhatFont("NIDialogMetaFontPrototypeBoldface");
	WhatFont("NIDialog");

	/*	char Font[256]="NIDialog";
	 *	int Bold=0, Italic=0;
	 *	SetFontPopupDefaults (0, 0, 0, 0, 0, VAL_LEFT_JUSTIFIED, VAL_BLACK, Size);
	 *	FontSelectPopup ("Select a base font", "Sample Text 0123", 0, Font,
	 *					 &Bold, 0, 0, &Italic, 0, 0, &Size, 6, 48, 1, 1);
	 *	CreateMetaFont("NIDialogMetaFontPrototypeBoldface", Font, Size, 1, 0, 0, 0);
	 *	CreateMetaFont("NIDialog", Font, Size, 0, 1, 0, 0);
	 *	//CreateMetaFont("NIDialogMetaFont", Font, Size, 0, 0, 0, 0);
	 */

	//FontSelectPopup ("Select a base font", "Sample Text 0123", 0, Font, 0,
	//				 0, 0, 0, 0, 0, &Size, 6, 48, 1, 1);

	CreateMetaFontWithCharacterSet ("MyFontBase", TypeFace, Size, 0, 0, 0, 0, 0, VAL_NATIVE_CHARSET);
	ReplaceFont(0, "NIDialogMetaFont", "MyFontBase");
	CreateMetaFontWithCharacterSet ("MyFontBold", TypeFace, Size, 1, 0, 0, 0, 0, VAL_NATIVE_CHARSET);
	ReplaceFont(0, "NIDialogMetaFontPrototypeBoldface", "MyFontBold");
	CreateMetaFontWithCharacterSet ("MyFontItalic", TypeFace, Size, 0, 1, 0, 0, 0, VAL_NATIVE_CHARSET);
	ReplaceFont(0, "NIDialog", "MyFontItalic");

	WhatFont("NIDialogMetaFont");
	WhatFont("NIDialogMetaFontPrototypeBoldface");
	WhatFont("NIDialog");
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Auto size and position of a set of horizontal controls such that
/// HIFN	[Box]Label [Box]Label [Box]Label [Box]Label...
/// HIFN	fits the panel width with all boxes of the same width
/// HIFN	MinWidth/Minimum width of the box ctrl (excluding label)
/// HIFN	MaxWidth/Maximum width of the box ctrl (excluding label, use 0 to disable)
/// HIPARV	List of controls
///////////////////////////////////////////////////////////////////////////////
void AutosizeControls(int Panel, int MinWidth, int MaxWidth, ...) {
	int i, Nb=0, Ctrl, W, WidthLabels=0, WidthPanel, WidthBox, Inter=4, Left=-1, R, LeftFirst=0;
	BOOL Visible;
	va_list str_args;

	// Count them
	va_start( str_args, MaxWidth );
	while (0!=va_arg( str_args, int)) Nb++;
	va_end( str_args );
	if (Nb==0) return;

	// get the label widths
	va_start( str_args, MaxWidth );
	for (i=0; i<Nb; i++) {
		Ctrl=va_arg( str_args, int);
		GetCtrlAttribute(Panel, Ctrl, ATTR_VISIBLE, &Visible);
		R=GetCtrlAttribute(Panel, Ctrl, ATTR_LABEL_WIDTH, &W);
		if (R>=0 and Visible) WidthLabels+=W+Inter;
		if (i==0)
			GetCtrlAttribute(Panel, Ctrl, ATTR_LEFT,  &LeftFirst);

	}
	va_end( str_args );

	GetPanelAttribute(Panel, ATTR_WIDTH, &WidthPanel);
	WidthBox=(WidthPanel-WidthLabels-LeftFirst)/Nb;
	if (               WidthBox<1) return;	// Too small
	if (               WidthBox<MinWidth) WidthBox=MinWidth;
	if (MaxWidth>0 and WidthBox>MaxWidth) WidthBox=MaxWidth;

	va_start( str_args, MaxWidth );
	for (i=0; i<Nb; i++) {
		Ctrl=va_arg( str_args, int);
		if (Left==-1) GetCtrlAttribute(Panel, Ctrl, ATTR_LEFT, &Left);
		else          SetCtrlAttribute(Panel, Ctrl, ATTR_LEFT,  Left);
		SetCtrlAttribute(Panel, Ctrl, ATTR_LEFT,  Left);
		SetCtrlAttribute(Panel, Ctrl, ATTR_WIDTH, WidthBox);
		GetCtrlAttribute(Panel, Ctrl, ATTR_LABEL_WIDTH, &W);
		GetCtrlAttribute(Panel, Ctrl, ATTR_VISIBLE, &Visible);
		if (Visible) {
			Left+=W+WidthBox+Inter;
			WidthLabels+=W;
		}
	}
	va_end( str_args );
}

#ifndef SIGN
#define SIGN(a) ((a)>0 ? 1 : (a)<0 ? -1 : 0)
#endif


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Nudge and enlarge/shrink a graph control so that its plot area is identical to another one
/// HIPAR	Pnl1/Panel of the reference graph
/// HIPAR	Graph1/Control of the reference graph
/// HIPAR	Pnl2/Panel of the graph to adjust
/// HIPAR	Graph2/Control of the graph to adjust
///////////////////////////////////////////////////////////////////////////////
void AlignHorizontalPlotAreas(int Pnl1, int Graph1, int Pnl2, int Graph2) {
	int Left, Width, AcqAreaLeft, AcqAreaWidth, ResidualLeft, ResidualAreaLeft, ResidualAreaWidth, Loop=0;
	// Align plot on Noise/precession plot
	GetCtrlAttribute(Pnl1, Graph1, ATTR_LEFT,  &Left);
	GetCtrlAttribute(Pnl1, Graph1, ATTR_WIDTH, &Width);
	SetCtrlAttribute(Pnl2, Graph2, ATTR_LEFT, ResidualLeft=Left);
	SetCtrlAttribute(Pnl2, Graph2, ATTR_WIDTH,Width);

	GetCtrlAttribute(Pnl1, Graph1, ATTR_PLOT_AREA_LEFT,  &AcqAreaLeft);
	GetCtrlAttribute(Pnl1, Graph1, ATTR_PLOT_AREA_WIDTH, &AcqAreaWidth);

	do {
		GetCtrlAttribute(Pnl2, Graph2, ATTR_PLOT_AREA_LEFT,  &ResidualAreaLeft);
		ResidualLeft += SIGN(Left+AcqAreaLeft - (ResidualLeft+ResidualAreaLeft));
		SetCtrlAttribute(Pnl2, Graph2, ATTR_LEFT, ResidualLeft);

		GetCtrlAttribute(Pnl2, Graph2, ATTR_PLOT_AREA_WIDTH, &ResidualAreaWidth);
		Width += SIGN(AcqAreaWidth-ResidualAreaWidth);
		SetCtrlAttribute(Pnl2, Graph2, ATTR_WIDTH, Width);
	} while (not (ResidualLeft+ResidualAreaLeft==Left+AcqAreaLeft and
					AcqAreaWidth==ResidualAreaWidth)
					and Loop++<100);
}



///////////////////////////////////////////////////////////////////////////////
/// HIFN	Copy the name of a tab to its corresponding panel title
/// HIFN	This is basically of use only in debugging since you cannot see the panel title bar
/// HIPAR	Overwrite/If FALSE, will copy the name only if the current panel title is empty
///////////////////////////////////////////////////////////////////////////////
void CopyNameFromTabToPanel(int panel, int control, int tab, BOOL Overwrite) {
	int Pnl=0, Len=0;
	char *Str;

	if (GetPanelHandleFromTabPage(panel, control, tab, &Pnl)<0) return;	// Wrong control ?
	if (!Overwrite) {
		GetPanelAttribute(Pnl, ATTR_TITLE_LENGTH, &Len);
		if (Len>0) return;
	}

	GetTabPageAttribute (panel, control, tab, ATTR_LABEL_TEXT_LENGTH, &Len);
	Str=malloc((size_t)Len+1);	if (Str==NULL) return;
	GetTabPageAttribute (panel, control, tab, ATTR_LABEL_TEXT, Str);
	SetPanelAttribute   (Pnl, ATTR_TITLE, Str);
	free(Str);
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Return TRUE if a Panel is part of a tab
///	HIPAR	Panel/Panel number
/// HIRET	TRUE if part of a tab, false if top-level or child panel
///////////////////////////////////////////////////////////////////////////////
BOOL IsPanelInTab(int Panel) {
	return FindParentTab(Panel, NULL)!=0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	If a panel is part of a tab, return that tab ctrl and index (the panel is actually the parent)
/// OUT		Index
///	HIPAR	Index/Return the index number of the panel within the tab. You may pass NULL
/// HIRET	Control number of the tab or 0 if no associated tab was found
///////////////////////////////////////////////////////////////////////////////
int FindParentTab(int Panel, int *Index) {
	int i, Parent=0, Style, Ctrl;
	if (Panel==0) return 0;

	GetPanelAttribute (Panel, ATTR_PANEL_PARENT, &Parent);
	if (Parent==0) return 0;	// It's a top-level panel

	// Find all tabs and see if that panel belong to them or if it's only a simple child panel
	GetPanelAttribute (Parent, ATTR_PANEL_FIRST_CTRL, &Ctrl);
	while (Ctrl!=0) {		// Find all controls belonging to the parent
		GetCtrlAttribute (Parent, Ctrl, ATTR_CTRL_STYLE, &Style);
		if (Style==CTRL_TABS) {
			i=GetTabIndexFromPanel(Parent, Ctrl, Panel);
			if (i>=0) { if (Index!=NULL) *Index=i; return Ctrl; }
		}
		GetCtrlAttribute (Parent, Ctrl, ATTR_NEXT_CTRL, &Ctrl);
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Set a panel (and its parent) active, or, if it is part of a tab, activates the tab
/// HIFN	This function is an almost drop-in replacement for SetActivePanel()
/// HIPAR	Panel/The panel which you want make active
/// HIPAR	Ctrl/If !=0, also activate that control, if possible
/// HIPAR	Force/If true and the top panel is not visible, it is displayed
/// HIPAR	Force/Set to 0 if you don't want a hidden panel to be forced into view
/// HIRET	0 if no associated tab was found, or its control number
///////////////////////////////////////////////////////////////////////////////
int SetActivePanelOrTab(int Panel, int Ctrl, int Force) {
	int i, Parent=0, Visible;
	if (Panel==0) return 0;

	if (Ctrl!=0) {
		i=SetBreakOnLibraryErrors(0);
		SetActiveCtrl(Panel, Ctrl);	// Because the control may be inoperable
		SetBreakOnLibraryErrors(i);
	}

	GetPanelAttribute (Panel, ATTR_PANEL_PARENT, &Parent);
	if (Parent==0) {
		GetPanelAttribute(Panel, ATTR_VISIBLE, &Visible);
		if (Visible or Force) {
			/*if (!Visible)*/ DisplayPanel(Panel),
			SetActivePanel(Panel);	// In any case, make the parent active (we could go on and on...)
		}
	} else {
		GetPanelAttribute(Parent, ATTR_VISIBLE, &Visible);
		if (Visible or Force) {
			/*if (!Visible)*/ DisplayPanel(Parent),
			SetActivePanel(Parent);	// In any case, make the parent active (we could go on and on...)
		}
		Ctrl=FindParentTab(Panel, &i);
		if (Ctrl==0) SetActivePanelOrTab(Panel, 0, Force);
		else { SetActiveTabPage(Parent, Ctrl, i); return Ctrl; }
	}
	return 0;
}



///////////////////////////////////////////////////////////////////////////////
/// HIFN	Example function to pass to FindAllCtrls
/// HIRET	Return non-zero to abort the exploration of all controls
///////////////////////////////////////////////////////////////////////////////
int CtrlFuncExample(int Pnl, int Ctrl) {
	int Style;
	GetCtrlAttribute (Pnl, Ctrl, ATTR_CTRL_STYLE, &Style);
	switch (Style) {
		case CTRL_DIGITAL_GRAPH:
		case CTRL_DIGITAL_GRAPH_LS:
		case CTRL_GRAPH:
		case CTRL_GRAPH_LS:
		case CTRL_STRIP_CHART:
		case CTRL_STRIP_CHART_LS:
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Example function to pass to FindAllCtrls
/// HIRET	Return non-zero to abort the exploration of all panels
///////////////////////////////////////////////////////////////////////////////
int PnlFuncExample(int Pnl) {
	char Str[255];
	GetPanelAttribute(Pnl, ATTR_TITLE, Str);
	fprintf(stderr, "\n%d: %s", Pnl, Str);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Find all controls on a given panel - Don't go into child panels
/// HIPAR	Pnl/Panel on which to find all the controls
/// HIPAR	CtrlFunc/An optional function to call on each control
/// HIRET	Number of controls position when aborted
///////////////////////////////////////////////////////////////////////////////
int FindAllCtrls(int Pnl, int CtrlFunc(int,int)) {
	int Nb=0, Ctrl=0;
	GetPanelAttribute (Pnl, ATTR_PANEL_FIRST_CTRL, &Ctrl);
	while (Ctrl>0) {
		Nb++;
		if (CtrlFunc!=NULL and CtrlFunc(Pnl, Ctrl)) return Nb;
		GetCtrlAttribute(Pnl, Ctrl, ATTR_NEXT_CTRL, &Ctrl);
	}
	return Nb;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Function which finds all open panels and fixes possible issues (debugging)
/// HIPAR	TopPanel/Start from that panel. 0 for all of them
/// HIPAR	Recurse/Descend into child panels as well
/// HIPAR	PnlFunc/An optional user defined function which is called for each panel
/// HIPAR	CtrlFunc/An optional user defined function which is called for each control
/// HIPAR	CtrlFunc/Pass NULL if you only want to explore panels
/// HIPAR	CtrlFunc/This function should return 0 unless you want to abort the search
/// HIRET	Number of child panels found
///////////////////////////////////////////////////////////////////////////////
int FindAllPanels(int TopPanel, BOOL Recurse,
				  int PnlFunc(int), int CtrlFunc(int, int)) {
	int Nb=0, Pnl;
	//	Pnl = GetActivePanel();
	GetPanelAttribute (TopPanel, ATTR_FIRST_CHILD, &Pnl);
	while (Pnl>0) {
		Nb++;
		if (PnlFunc!=NULL  and PnlFunc(Pnl)) return Nb;
		if (CtrlFunc!=NULL and FindAllCtrls(Pnl, CtrlFunc)) return Nb;

		// Find SubPanels recursively
		if (Recurse) Nb+=FindAllPanels(Pnl, Recurse, PnlFunc, CtrlFunc);

		GetPanelAttribute (Pnl, ATTR_NEXT_PANEL, &Pnl);	// Next one at same level
	}
	return Nb;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Find a control knowing its constant name
/// HIPAR	Pnl/Panel on which to find the control
/// HIPAR	ConstName/Constant name of the control, excluding the prepended panel constant
/// HIRET	Control number, 0 if not found
//  TODO	Add an option to add panel constant name
///////////////////////////////////////////////////////////////////////////////
int LookForCtrlFromConstName(int Pnl, const char* ConstName) {
	int Ctrl=0;
	char Name[80];
	GetPanelAttribute (Pnl, ATTR_PANEL_FIRST_CTRL, &Ctrl);
	while (Ctrl>0) {
		GetCtrlAttribute (Pnl, Ctrl, ATTR_CONSTANT_NAME, Name);
		if (strcmp(ConstName, Name)==0) return Ctrl;
		GetCtrlAttribute(Pnl, Ctrl, ATTR_NEXT_CTRL, &Ctrl);
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Place the help label near the cursor
/// HIPAR	X/Mouse horizontal position
/// HIPAR	Y/Mouse vertical position
///////////////////////////////////////////////////////////////////////////////
void PlaceNearCursor(int Panel, int Control, int X, int Y) {
	int W, H, CtrlH, CtrlW;
	GetPanelAttribute(Panel, ATTR_WIDTH,  &W);
	GetPanelAttribute(Panel, ATTR_HEIGHT, &H);

	GetCtrlAttribute(Panel, Control, ATTR_WIDTH,  &CtrlW);
	GetCtrlAttribute(Panel, Control, ATTR_HEIGHT, &CtrlH);

	SetCtrlAttribute(Panel, Control, ATTR_LEFT, X<W/2 ? X+1 : X-CtrlW-1);
	SetCtrlAttribute(Panel, Control, ATTR_TOP,  Y<H/2 ? Y+1 : Y-CtrlH-1);
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Return the position of the center of a control
/// OUT		X, Y
/// HIPAR	X/Horizontal position
/// HIPAR	Y/Vertical position
///////////////////////////////////////////////////////////////////////////////
void GetCenterOfCtrl(int Panel, int Control, int *X, int *Y) {
	int Left, Top, Width, Height;
	*X=*Y=0;
	GetCtrlAttribute(Panel, Control, ATTR_LEFT,  &Left);
	GetCtrlAttribute(Panel, Control, ATTR_TOP,   &Top);
	GetCtrlAttribute(Panel, Control, ATTR_WIDTH, &Width);
	GetCtrlAttribute(Panel, Control, ATTR_HEIGHT,&Height);
	*X=Left+Width/2;
	*Y=Top+Height/2;
	return;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Simply add this call to your graph callback functions:
/// HIFN	case EVENT_MOUSE_WHEEL_SCROLL:
/// HIFN		ScrollWheelZoom(panel, control, eventData1);
/// HIFN		break;
///////////////////////////////////////////////////////////////////////////////
void ScrollWheelZoom(int panel, int control, int eventData1) {
	double X0, X1, Y0, Y1;
	int CtrlX, CtrlY, KeyModifiers;
	int PaH, PaW, PaT, PaL;
	#define Factor (4./3)
	GetRelativeMouseState(panel, control, &CtrlX, &CtrlY, NULL, NULL, &KeyModifiers);
	GetAxisScalingMode(panel, control, VAL_LEFT_YAXIS,   NULL, &Y0, &Y1);
	GetAxisScalingMode(panel, control, VAL_BOTTOM_XAXIS, NULL, &X0, &X1);
	GetCtrlAttribute  (panel, control, ATTR_PLOT_AREA_HEIGHT,&PaH);
	GetCtrlAttribute  (panel, control, ATTR_PLOT_AREA_WIDTH, &PaW);
	GetCtrlAttribute  (panel, control, ATTR_PLOT_AREA_TOP,   &PaT);
	GetCtrlAttribute  (panel, control, ATTR_PLOT_AREA_LEFT,  &PaL);
	if (CtrlX<PaL or CtrlX>PaL+PaW or
		CtrlY<PaT or CtrlY>PaT+PaH) return;	// Out of plot area
	double Y=Y0-(Y1-Y0)*(CtrlY-PaT-PaH)/PaH;
	double X=X0+(X1-X0)*(CtrlX-PaL)/PaW;
	if ( eventData1 == MOUSE_WHEEL_SCROLL_DOWN ) {
		X0=X+(X0-X)*Factor; X1=X+(X1-X)*Factor;
		Y0=Y+(Y0-Y)*Factor; Y1=Y+(Y1-Y)*Factor;
	} else if ( eventData1 == MOUSE_WHEEL_SCROLL_UP ) {
		X0=X+(X0-X)/Factor; X1=X+(X1-X)/Factor;
		Y0=Y+(Y0-Y)/Factor; Y1=Y+(Y1-Y)/Factor;
	}
	SetAxisScalingMode(panel, control, VAL_LEFT_YAXIS,   VAL_MANUAL, Y0, Y1);
	SetAxisScalingMode(panel, control, VAL_BOTTOM_XAXIS, VAL_MANUAL, X0, X1);
	SetAxisScalingMode(panel, control, VAL_LEFT_YAXIS,   VAL_LOCK, 0, 0);
	SetAxisScalingMode(panel, control, VAL_BOTTOM_XAXIS, VAL_LOCK, 0, 0);
}


///////////////////////////////////////////////////////////////////////////////
/* The following 2 functions switch between SDI and MDI
*   SDI: Single Document Interface   - The panel is top-level
*   MDI: Multiple Document Interface - The panel is in a Tab
*   Have a panel with a tab ready
*   Place a picture ring (without inc/dec arrows) in the panel you wish to (un)tab
*   and use the following callback function on it:
* int  CVICALLBACK cb_TabToPanel(int panel, int control, int event,
*							   void *callbackData, int eventData1, int eventData2) {
*	BOOL ToTab;
*	switch (event) {
*		case EVENT_LEFT_CLICK:
*			GetCtrlVal(panel, control, &ToTab);
*			SetCtrlVal(panel, control, ToTab=!ToTab);
*			if (ToTab) MovePanelToTab(pTab, PTAB_TAB, &PnlHandle); // And not 'panel' as the value will change
*			else PnlHandle=MovePanelFromTab(pTab, PTAB_TAB,
*						PnlHandle==0 ? -1 : GetTabIndexFromPanel(pTab, PTAB_TAB, PnlHandle));
*			break;
*	}
*	return 0;
* }
*/

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Take an existing top-level panel and pushes it inside a tab (at the last position)
/// OUT		Panel
/// HIPAR	Panel/Panel to put inside a tab.
/// HIPAR	Panel/THE HANDLE WILL CHANGE, so use a variable, not a callback function parameter
/// HIPAR	pTab/Panel containing the tab
/// HIPAR	TabCtrl/Handle of the tab control
/// HIRET	Index of the newly created tab
///////////////////////////////////////////////////////////////////////////////
int MovePanelToTab(int pTab, int TabCtrl, int *Panel) {
	//	int Old=*Panel;
	if (*Panel<0)
		return -1;
	int NewTabIndex = InsertPanelAsTabPage (pTab, TabCtrl, -1, *Panel);
	DiscardPanel(*Panel);
	GetPanelHandleFromTabPage (pTab, TabCtrl, NewTabIndex, Panel);
	SetCtrlAttribute (pTab, TabCtrl, ATTR_CTRL_INDEX, NewTabIndex);
	//	printf("\nPanel %d -> idx %d (NewPnl %d)", Old, NewTabIndex, *Panel);
	return NewTabIndex;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Takes a tab and turn it into an independant panel
/// HIFN	Note that it removes it from the tab, so the index of the next tabs will change
/// HIPAR	pTab/Panel containing the tab
/// HIPAR	TabCtrl/Handle of the tab control
/// HIPAR	index/index of the tab to turn into a panel (will be deleted) or -1 to use active one
/// HIRET	Panel handle of newly independant tab
/// HIRET	- note that it is different from the handle obtained from the tab
///////////////////////////////////////////////////////////////////////////////
int MovePanelFromTab(int pTab, int TabCtrl, int index) {
	int Pnl;
	if (index<0) GetActiveTabPage (pTab, TabCtrl, &index);
	GetPanelHandleFromTabPage (pTab, TabCtrl, index, &Pnl);
	int NewPnl = DuplicatePanel (0, Pnl, 0, VAL_KEEP_SAME_POSITION, VAL_KEEP_SAME_POSITION);
	//	printf("\nPanel %d <- idx %d (was %d)", NewPnl, index, Pnl);
	SetPanelAttribute(NewPnl, ATTR_TITLEBAR_VISIBLE, 1);

	CopyNameFromTabToPanel(pTab, TabCtrl, index, FALSE);

	DeleteTabPage (pTab, TabCtrl, index, 1);
	DisplayPanel  (NewPnl);
	SetActivePanel(NewPnl);
	return NewPnl;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Get the index of a panel that's inside a tab
/// HIPAR	pTab/Panel containing the tab
/// HIPAR	TabCtrl/Handle of the tab control
/// HIPAR	Panel/Handle of the panel to look for within that tab
/// HIRET	The tab index of the panel, or -1 if not found
///////////////////////////////////////////////////////////////////////////////
int GetTabIndexFromPanel(int pTab, int TabCtrl, int Panel) {
	int Count, i, Pnl;
	GetNumTabPages (pTab, TabCtrl, &Count);
	for (i=0; i<Count; i++) {
		GetPanelHandleFromTabPage (pTab, TabCtrl, i, &Pnl);
		if (Pnl==Panel) { /*printf("\nPanel %d    idx %d", Panel, i);*/ return i; }
	}
	//	printf("\nPanel %d    idx %d", Panel, -1);
	return -1;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Just like SetCtrlVal for controls that accept a string, but with a printf format
/// HIPAR	Panel/Panel handle
/// HIPAR	Control/Control handle. This control must accept a string as a value. No sanity check is performed
/// HIPARV	fmt/Printf-style format and associate parameters
/// HIRET	-1 if out of mem, SetCtrlVal otherwise
///////////////////////////////////////////////////////////////////////////////
int SetCtrlValPrintf(const int Panel, const int Control, const char *fmt, ... ) {
	va_list str_args;
	va_start( str_args, fmt );
	#pragma clang diagnostic push
	#pragma clang diagnostic ignored "-Wformat"
	int Nb = vsnprintf (NULL, 0, fmt, str_args);
	char *Message=malloc(Nb+2);
	if (Message==NULL) return UIEOutOfMemory;
	vsnprintf(Message, Nb+1, fmt, str_args);
	#pragma clang diagnostic pop
	va_end( str_args );

	int R=SetCtrlVal(Panel, Control, Message);
	free(Message);
	return R;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Just like SetCtrlAttribute for controls that accept a string, but with a printf format
/// HIPAR	Panel/Panel handle
/// HIPAR	Control/Control handle. This control must accept a string as an attribute. No sanity check is performed
/// HIPAR	Attr/Attribute
/// HIPARV	fmt/Printf-style format and associate parameters
/// HIRET	-1 if out of mem, SetCtrlVal otherwise
///////////////////////////////////////////////////////////////////////////////
int SetCtrlAttrPrintf(const int Panel, const int Control, const int Attr, const char *fmt, ... ) {
	va_list str_args;
	va_start( str_args, fmt );
	#pragma clang diagnostic push
	#pragma clang diagnostic ignored "-Wformat"
	int Nb = vsnprintf (NULL, 0, fmt, str_args);
	char *Message=malloc(Nb+2);
	if (Message==NULL) return UIEOutOfMemory;
	vsnprintf(Message, Nb+1, fmt, str_args);
	#pragma clang diagnostic pop
	va_end( str_args );

	int R=SetCtrlAttribute(Panel, Control, Attr, Message);
	free(Message);
	return R;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Just like SetCtrlAttribute for controls that accept a string, but with a printf format
/// HIPAR	Panel/Panel handle
/// HIPAR	Control/Control handle. This control must accept a string as an attribute. No sanity check is performed
/// HIPAR	Attr/Attribute
/// HIPARV	fmt/Printf-style format and associate parameters
/// HIRET	-1 if out of mem, SetCtrlVal otherwise
///////////////////////////////////////////////////////////////////////////////
int SetPlotAttrPrintf(const int Panel, const int Control, const int Plot, const int Attr, const char *fmt, ... ) {
	va_list str_args;
	va_start( str_args, fmt );
	#pragma clang diagnostic push
	#pragma clang diagnostic ignored "-Wformat"
	int Nb = vsnprintf (NULL, 0, fmt, str_args);
	char *Message=malloc(Nb+2);
	if (Message==NULL) return UIEOutOfMemory;
	vsnprintf(Message, Nb+1, fmt, str_args);
	#pragma clang diagnostic pop
	va_end( str_args );

	int R=SetPlotAttribute(Panel, Control, Plot, Attr, Message);
	free(Message);
	return R;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Just like SetPanelAttribute for attributes that accept a string, but with a printf format
/// HIPAR	Panel/Panel handle
/// HIPAR	Attr/Attribute
/// HIPARV	fmt/Printf-style format and associate parameters
/// HIRET	-1 if out of mem, return from SetPanelAttribute otherwise
///////////////////////////////////////////////////////////////////////////////
int SetPanelAttrPrintf(const int Panel, const int Attr, const char *fmt, ... ) {
	va_list str_args;
	va_start( str_args, fmt );
	#pragma clang diagnostic push
	#pragma clang diagnostic ignored "-Wformat"
	int Nb = vsnprintf (NULL, 0, fmt, str_args);
	char *Message=malloc(Nb+2);
	if (Message==NULL) return UIEOutOfMemory;
	vsnprintf(Message, Nb+1, fmt, str_args);
	#pragma clang diagnostic pop
	va_end( str_args );

	int R=SetPanelAttribute(Panel, Attr, Message);
	free(Message);
	return R;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Just like MessagePopup, but with a printf format
/// HIPAR	Title/Of the popup
/// HIPARV	fmt/Printf-style format and associate parameters
/// HIRET	Possible MessagePopup error code
///////////////////////////////////////////////////////////////////////////////
int PrintfPopup(char* Title, const char *fmt, ... ) {
	va_list str_args;
	va_start( str_args, fmt );
	#pragma clang diagnostic push
	#pragma clang diagnostic ignored "-Wformat"
	int Nb = vsnprintf (NULL, 0, fmt, str_args);
	char *Message=malloc(Nb+2);
	if (Message==NULL) return UIEOutOfMemory;
	vsnprintf(Message, Nb+1, fmt, str_args);
	#pragma clang diagnostic pop
	va_end( str_args );

	int R=MessagePopup (Title, Message);
	free(Message);
	return R;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Just like perror(), but as a popup box. This uses errno
/// HIPARV	fmt/Printf-style optional message to display in addition to the error message
/// HIRET	Unlikely error code from MessagePopup
///////////////////////////////////////////////////////////////////////////////
int PerrorPopup(const char *fmt, ...) {
	va_list str_args;
	va_start( str_args, fmt );
	#pragma clang diagnostic push
	#pragma clang diagnostic ignored "-Wformat"
	int Nb = vsnprintf (NULL, 0, fmt, str_args);
	char *Message=malloc(Nb+3+strlen(strerror(errno)));
	if (Message==NULL) return UIEOutOfMemory;
	int Writ=sprintf(Message, "%s\n", strerror(errno));
	vsnprintf(Message+Writ, Nb+1, fmt, str_args);
	#pragma clang diagnostic pop
	va_end( str_args );

	int R=MessagePopup("Error", Message);
	free(Message);
	return R;
}


///////////////////////////////////////////////////////////////////////////////
#ifdef TEST_PM
#include <cvirte.h>

int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;    /* out of memory */
	errno=1;
	PerrorPopup("This is a test");
	return 0;
}

#endif

