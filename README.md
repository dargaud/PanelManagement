This is a set of functions for managing LabWindows/CVI panels.
Just add it to you CVI programs and see the .h for help
There's no test case or examples, just an empty main() you can force
with the compilation option /DTEST_PM
