#ifndef __PANEL_MANAGEMENT_H
#define __PANEL_MANAGEMENT_H

#include <userint.h>

// Used to remember a user selection
extern char SelectedTypeface[256];
extern int  SelectedFontSize;

// Directory where to save screenshots obtained with Ctrl-B
extern char ScreenShotDir[MAX_PATHNAME_LEN];
// Processes standard keys: Ctrl-P to print, Ctrl-B for a screenshot, etc...
extern int HandlePanelDefaultKeys(int panel, int eventData1) __attribute__((deprecated("Use HandlePanelDefaultKeysExt() instead")));
extern int HandlePanelDefaultKeysExt(int panel, int eventData1, int eventData2);
extern int GetMyKey(int eventData2);

// Go between visible panels while ignoring tabs
extern void ShowPreviousPanel(int panel);
extern void ShowNextPanel    (int panel);

// Align graphics
extern void AlignHorizontalPlotAreas(int Pnl1, int Graph1, int Pnl2, int Graph2);

// Center panel in closest monitor
extern void CenterPanel(int panel);
extern void GetPanelAbsolutePos(int Pnl, int* Top, int* Left);

// Replace a font with another
extern void ReplaceFont(int panel, char *Find, char *ReplaceWith);
extern void SetFontAttribute(int panel, int FontAttr, char *ReplaceWith);
extern void FixFonts(char *TypeFace, int Size);

// Size and distribute a bunch of controls
extern void AutosizeControls(int Panel, int MinWidth, int MaxWidth, ...);


///////////////////////////////////////////////////////////////////////////////
// Call a user function for each panel and/or control
extern int FindAllCtrls (int Pnl, int CtrlFunc(int,int));
extern int FindAllPanels(int TopPanel, int Recurse,
						 int PnlFunc(int), int CtrlFunc(int,int));

// Find a control knowing its constant name
extern int LookForCtrlFromConstName(int Pnl, const char* ConstName);

// Place a control near a coordinate
extern void PlaceNearCursor(int Panel, int Control, int  X, int  Y);
// Get center coordinate
extern void GetCenterOfCtrl(int Panel, int Control, int *X, int *Y);

// Zoom a graph with the mouse scroll wheel
extern void ScrollWheelZoom(int panel, int control, int eventData1);

///////////////////////////////////////////////////////////////////////////////
// The following deal with panels within tabs:

// Make a panel active, or if it's in a tab, make the tab active
extern int SetActivePanelOrTab(int Panel, int Ctrl, int Force);
// Find if a panel is part of a tab
extern BOOL IsPanelInTab(int Panel);
extern int FindParentTab(int Panel, int *Index);
// Get the index of a panel in a tab
extern int GetTabIndexFromPanel(int pTab, int TabCtrl, int Panel);
// Move panel in or out of tab
extern int MovePanelToTab      (int pTab, int TabCtrl, int*Panel);
extern int MovePanelFromTab    (int pTab, int TabCtrl, int index);
extern void CopyNameFromTabToPanel(int panel, int control, int tab, int Overwrite);


///////////////////////////////////////////////////////////////////////////////
// The following give a printf-like method instead of a simple string

extern int SetCtrlValPrintf(const int Panel, const int Control, const char *fmt, ... )
	__attribute__ (( __format__( __printf__, 3, 4 ) ));
extern int SetCtrlAttrPrintf(const int Panel, const int Control, const int Attr, const char *fmt, ... )
	__attribute__ (( __format__( __printf__, 4, 5 ) ));
extern int SetPlotAttrPrintf(const int Panel, const int Control, const int Plot, const int Attr, const char *fmt, ... )
	__attribute__ (( __format__( __printf__, 5, 6 ) ));
extern int SetPanelAttrPrintf(const int Panel, const int Attr, const char *fmt, ... )
	__attribute__ (( __format__( __printf__, 3, 4 ) ));
extern int InsertListItemPrintf(int panel, int control,  int  index, const char *itemLabel, ...);

// There are many other possibilities: SetCtrlArrayVal, SetCtrlArrayAttribute,
//	SetCtrlMenuAttribute, SetMenuBarAttribute, SetRingItemAttribute, SetSystemAttribute,
//	SetTableCellAttribute, SetTableCellVal, SetTableRowAttribute, SetTabPageAttribute
//	SetTreeItemAttribute, FillTableCellRange,
//	InsertListItem, ReplaceListItem, SetAnnotationAttribute
// and surely some others. But some would be difficult to translate as
// they have *another* variadic argument (for instance InsertListItem).

extern int PrintfPopup(char* Title, const char *fmt, ... )
	__attribute__ (( __format__( __printf__, 2, 3 ) ));
extern int PerrorPopup(const char *fmt, ... )
	__attribute__ (( __format__( __printf__, 1, 2 ) ));

#endif
